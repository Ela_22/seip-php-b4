
<!DOCTYPE HTML>
<html>

<head>
  <title>simplestyle_purple</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="{{asset('backend/style/style.css')}}" />
</head>

<body>
  <div id="main">
<x-frontend.layouts.partials.navbar>
    <!-- navbar -->
</x-frontend.layouts.partials.navbar>
    <div id="content_header">
        {{$slot}}
        <!-- sobi rakhar jayga -->
    </div>
   
    </div>
    <div id="content_footer"></div>
    <x-frontend.layouts.partials.footer>
        <!-- footer -->
    </x-frontend.layouts.partials.footer>
  </div>
</body>
</html>
