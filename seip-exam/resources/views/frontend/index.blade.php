<x-backend.layouts.master>
    <h1 class="mt-4">Products</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">products</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Product List

            
            <a class="btn btn-sm btn-primary" href="{{ route('products.add') }}">Add New</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($products as $product)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->price }} TK</td>
                        
                        <td>

                            @can('product-show', $product)
                            <a class="btn btn-info btn-sm" href="{{ route('products.show', ['product' => $product->id]) }}">Show</a>    
                            @endcan

                            @can('product-edit', $product)
                            <a class="btn btn-warning btn-sm" href="{{ route('products.edit', ['product' => $product->id]) }}">Edit</a>    
                            @endcan
                            
                            @can('product-delete', $product)
                            <form action="{{ route('products.destroy', ['product' => $product->id]) }}" method="POST" 
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>
                            @endcan

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-frontend.layouts.master>